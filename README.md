# A new Netbeans antlr plugin

![screen1](https://gitlab.com/mcheung63/netbeans-antlr/raw/screenshot/screen1.png)

![screen2](https://gitlab.com/mcheung63/netbeans-antlr/raw/screenshot/screen2.png)

![screen3](https://gitlab.com/mcheung63/netbeans-antlr/raw/screenshot/screen3.png)

# Architectural diagram

![](https://peter.quantr.hk/wp-content/uploads/2019/07/architecture.png)
