// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.syntax.antlr4.Antlr4LanguageHierarchy;
import java.io.File;
import java.io.FileInputStream;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestANTLRv4Lexer {

	@Test
	public void test() throws Exception {
		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(new FileInputStream(new File(".").getAbsolutePath() + "/../Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerParser.g4")));
		org.antlr.v4.runtime.Token token;

		do {
			token = lexer.nextToken();
			if (token.getType() != ANTLRv4Lexer.EOF) {
				String tokenName = Antlr4LanguageHierarchy.getToken(token.getType()).name();
				String t = token.getText();
				System.out.printf("%-30s %s\n", tokenName, t.substring(0, t.length() > 10 ? 10 : t.length()));
			}
		} while (token.getType() != ANTLRv4Lexer.EOF);
	}
}
