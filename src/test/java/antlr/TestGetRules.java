// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.antlr.v4.Tool;
import org.antlr.v4.parse.ANTLRParser;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.ast.GrammarRootAST;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestGetRules {

	@Test
	public void test() throws Exception {
		String content = new String(Files.readAllBytes(Paths.get(getClass().getResource("RISCVAssemblerParser.g4").toURI())));
		Tool tool = new Tool();
		GrammarRootAST ast = tool.parseGrammarFromString(content);

		if (ast.grammarType == ANTLRParser.COMBINED || ast.grammarType == ANTLRParser.PARSER) {
//				Grammar grammar = tool.createGrammar(ast);
//				tool.process(grammar, false);
			Grammar grammar = new Grammar(content);
			for (String rule : grammar.getRuleNames()) {
				System.out.println(rule);
			}
		}
	}
}
