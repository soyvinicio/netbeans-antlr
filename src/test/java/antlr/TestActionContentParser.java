// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.antlraction.ActionContentListener;
import hk.quantr.netbeans.antlr.syntax.antlr4.MyANTLRv4ParserListener;
import java.io.File;
import java.io.FileInputStream;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestActionContentParser {

	@Test
	public void test() throws Exception {
		System.out.println(new File(".").getAbsolutePath());
		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(new FileInputStream(new File(".").getAbsolutePath() + "/../Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerParser.g4")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
		ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();
		ParseTreeWalker walker = new ParseTreeWalker();
		ActionContentListener listener = new ActionContentListener();
		walker.walk(listener, context);
	}
}
