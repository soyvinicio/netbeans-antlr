package hk.quantr.netbeans.antlr.syntax.antlr4;

import java.util.List;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public enum AstSerializer {

	INSTANCE;

	public String toDot(Ast ast, boolean rulesOnly) {
		List<AstNode> nodes = ast.getNodes();
		StringBuilder sb = new StringBuilder()
				.append("graph {\n")
				.append("\tnode [fontname=Helvetica,fontsize=11];\n")
				.append("\tedge [fontname=Helvetica,fontsize=10];\n");

		for (AstNode n : nodes) {
			sb.append("\tn");
			sb.append(n.getId());
			sb.append(" [label=\"");
			if (!rulesOnly) {
				sb.append("(");
				sb.append(n.getId());
				sb.append(")\\n");
				sb.append(n.getEscapedLabel());
				sb.append("\\n");
			}
			sb.append(n.getName());
			sb.append("\"];\n");
		}

		nodes.forEach(n -> n.getChildren().stream()
				.filter(AstNode::hasParent)
				.forEach(c -> sb
				.append("\tn")
				.append(c.getParent().getId())
				.append(" -- n")
				.append(c.getId())
				.append(";\n")));

		sb.append("}\n");

		return sb.toString();
	}

	public String toDot(Ast ast) {
		return toDot(ast, false);
	}

}
