package hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile;

import java.awt.Dimension;
import java.io.File;
import java.util.HashMap;
import javax.swing.JOptionPane;
import org.openide.loaders.DataObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ChooseRealTimecompileFilePanel extends javax.swing.JPanel {

	public static HashMap<DataObject, File> maps = new HashMap<>();

	RealTimeComboModel realTimeComboModel = new RealTimeComboModel();
	public String compileErrorMessage;

	public ChooseRealTimecompileFilePanel() {
		initComponents();
		setMaximumSize(new Dimension(150, 25));
		setPreferredSize(new Dimension(150, 25));
	}

	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        compileStatusLabel = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setLayout(new java.awt.GridBagLayout());

        compileStatusLabel.setForeground(java.awt.Color.white);
        compileStatusLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                compileStatusLabelMouseClicked(evt);
            }
        });
        add(compileStatusLabel, new java.awt.GridBagConstraints());
    }// </editor-fold>//GEN-END:initComponents

    private void compileStatusLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_compileStatusLabelMouseClicked
		JOptionPane.showMessageDialog(null, compileErrorMessage);
    }//GEN-LAST:event_compileStatusLabelMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel compileStatusLabel;
    // End of variables declaration//GEN-END:variables

}
