// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package hk.quantr.netbeans.antlr.antlraction;

import org.antlr.parser.ANTLRv4Parser;
import org.antlr.parser.ANTLRv4ParserBaseListener;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ActionContentListener extends ANTLRv4ParserBaseListener {

	@Override
	public void enterAction(ANTLRv4Parser.ActionContext ctx) {
		System.out.println(ctx.getText());
	}

	@Override
	public void exitAction(ANTLRv4Parser.ActionContext ctx) {
	}
}
